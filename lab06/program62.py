from BeautifulSoup import BeautifulSoup as Soup
import csv
import urllib
data= urllib.urlopen("http://www.superherodb.com/superman/10-791/").read()
soup=Soup((data))
writer =csv.writer(open('superman.csv','w'))
writer.writerow(["NAME", " INTELLIGENCE", " STRENGTH", " SPEED", " DURABILITY", " POWER", " COMBAT"])
lst=["Superman"]
for x in  soup.findAll("div",{"class": "cblock"})[0].findAll("div",{"role": "progressbar"}):
     lst.append(x.get("aria-valuenow"))
writer.writerow(lst)
