from BeautifulSoup import BeautifulSoup as Soup
import csv
import urllib
data1= urllib.urlopen("http://www.superherodb.com/characters/").read()
soup1=Soup((data1))
writer =csv.writer(open('superheroes.csv','w'))
writer.writerow(["NAME", " INTELLIGENCE", " STRENGTH", " SPEED", " DURABILITY", " POWER", " COMBAT"])

for x in soup1.findAll("div",{"class": "cblock"})[1].findAll("li"):
    
    soup = Soup(urllib.urlopen("http://www.superherodb.com%s" % x.a.get("href")).read())
    try:
      x.find('span').extract()
    except Exception:
      pass
    lst=[str(x.a.text)]
    for x in  soup.findAll("div",{"class": "cblock"})[0].findAll("div",{"role": "progressbar"}):
        lst.append(x.get("aria-valuenow"))
    writer.writerow(lst)
