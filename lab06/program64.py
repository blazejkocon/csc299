import csv
import operator


writer =csv.writer(open('ranking.csv','w'))
writer.writerow(["RANK"," NAME"," TOTAL_POWERS"])
reader = csv.reader(open("superheroes.csv","r"))
result = [row for row in reader][1:]
d={}    

for r in result:
    if len(r)>1:
        s = sum(int(i) for i in r[1:]  if i.isdigit())
        d[r[0]]=s
d=sorted(d.items(), key=operator.itemgetter(1), reverse=True)
for i,key in enumerate(d,start=1):
    writer.writerow([i,key[0],key[1]])
    

