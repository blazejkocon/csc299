from BeautifulSoup import BeautifulSoup as Soup
import json
import re
d={}
soup=Soup(open("CSC.properties.1.xml","r").read())

for dtag in soup.findAll(name=re.compile('^d\:\w+')):
    d[dtag.name[2:]]=dtag.string

json.dump(d,open('CSC.properties.1.json','w'))
