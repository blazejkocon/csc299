import urllib
import csv

d={}
x=open("totals.csv","w")
x.write("USER_ID, TOTAL_EXPENSE\n")
reader = csv.reader(open("expenses.csv","r"))
result = [row for row in reader][1:]

for i in result:
    if i[0] in d:
        d[i[0]]+=float(i[1].strip("$"))
    else:
        d[i[0]]=float(i[1].strip("$"))

    
for item in sorted(d):

    x.write(item + ",$"+str(d[item])+'\n')

